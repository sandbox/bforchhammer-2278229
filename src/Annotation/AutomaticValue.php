<?php
/**
 * @file
 * Contains Drupal\entity_labels\Annotation\AutomaticValue.
 */

namespace Drupal\entity_labels\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a AutomaticValue annotation object.
 *
 * @Annotation
 */
class AutomaticValue extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the automatic value plugin.
   *
   * @ingroup plugin_translatable
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $label;

  /**
   * A short description of the automatic value plugin.
   *
   * @ingroup plugin_translatable
   *
   * @var \Drupal\Core\Annotation\Translation (optional)
   */
  public $description = '';

}
