<?php
/**
 * @file
 * Contains \Drupal\entity_labels\Access\AutomaticValueResetCheck
 */
namespace Drupal\entity_labels\Access;

use Drupal\Core\Access\AccessInterface;
use Drupal\entity_labels\EntityLabelsManagerInterface;
use Drupal\entity_labels\Exception\InvalidTypeException;

/**
 * Class AutomaticValueResetCheck
 */
class AutomaticValueResetCheck implements AccessInterface {
  /**
   * The entity labels manager service.
   *
   * @var \Drupal\entity_labels\EntityLabelsManagerInterface
   */
  protected $entityLabels;

  /**
   * Constructs a AutomaticValueResetCheck object.
   *
   * @param \Drupal\entity_labels\EntityLabelsManagerInterface $entity_labels
   *   The entity labels manager.
   */
  public function __construct(EntityLabelsManagerInterface $entity_labels) {
    $this->entityLabels = $entity_labels;
  }

  /**
   * Check access for resetting an automatic value.
   */
  public function access($type = NULL) {
    try {
      $this->entityLabels->validateTypeId($type);
    } catch (InvalidTypeException $e) {
      return static::KILL;
    }

    $autolabel = $this->entityLabels->loadAutomaticValue($type, FALSE);
    if (empty($autolabel) || !$autolabel->isPersistent()) {
      return static::KILL;
    }
    return static::ALLOW;
  }
}
