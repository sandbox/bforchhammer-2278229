<?php
/**
 * @file
 * Contains \Drupal\entity_labels\ConfigTranslationMapper.
 */

namespace Drupal\entity_labels;

use Drupal\config_translation\ConfigMapperManagerInterface;
use Drupal\config_translation\ConfigNamesMapper;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\locale\LocaleConfigManager;
use Symfony\Cmf\Component\Routing\RouteProviderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Configuration translation mapper for automatic values.
 */
class ConfigTranslationMapper extends ConfigNamesMapper {

  protected $entityLabels;
  protected $typeId;

  /**
   * @param array $plugin_id
   * @param mixed $plugin_definition
   * @param ConfigFactoryInterface $config_factory
   * @param LocaleConfigManager $locale_config_manager
   * @param ConfigMapperManagerInterface $config_mapper_manager
   * @param RouteProviderInterface $route_provider
   * @param TranslationInterface $translation_manager
   * @param EntityLabelsManagerInterface $entity_labels
   */
  public function __construct($plugin_id, $plugin_definition, ConfigFactoryInterface $config_factory, LocaleConfigManager $locale_config_manager, ConfigMapperManagerInterface $config_mapper_manager, RouteProviderInterface $route_provider, TranslationInterface $translation_manager, EntityLabelsManagerInterface $entity_labels) {
    parent::__construct($plugin_id, $plugin_definition, $config_factory, $locale_config_manager, $config_mapper_manager, $route_provider, $translation_manager);

    $this->entityLabels = $entity_labels;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('locale.config.typed'),
      $container->get('plugin.manager.config_translation.mapper'),
      $container->get('router.route_provider'),
      $container->get('string_translation'),
      $container->get('entity_labels')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function populateFromRequest(Request $request) {
    parent::populateFromRequest($request);
    $this->setTypes($request->attributes->get('type'));
  }

  /**
   * Sets the current types, and adds respective config names.
   *
   * @param string $type
   */
  public function setTypes($type) {
    $this->typeId = $type;

    $autovalue = $this->entityLabels->loadAutomaticValue($this->typeId, FALSE);
    if (!empty($autovalue) && $autovalue->isPersistent()) {
      list($entity_type, $bundle) = EntityLabelsManager::explodeType($this->typeId);
      $this->addConfigName(AutomaticValueManager::getConfigKey($entity_type, $bundle));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function hasTranslatable() {
    if (empty($this->pluginDefinition['names'])) {
      return FALSE;
    }
    return parent::hasTranslatable();
  }

  /**
   * {@inheritdoc}
   */
  public function getBaseRouteParameters() {
    return array(
      'type' => $this->typeId,
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    return $this->t('!type automatic value', array(
      '!type' => $this->entityLabels->getPropertyLabel($this->typeId)
    ));
  }

  /**
   * {@inheritdoc}
   */
  public function getTypeName() {
    return $this->t('Entity Labels Automatic Value');
  }

  /**
   * {@inheritdoc}
   */
  public function getTypeLabel() {
    return $this->t('Automatic Value');
  }

  /**
   * {@inheritdoc}
   */
  public function getOperations() {
    return array(
      'list' => array(
        'title' => $this->t('List'),
        'href' => 'admin/structure/entity-labels',
      ),
    );
  }
}
