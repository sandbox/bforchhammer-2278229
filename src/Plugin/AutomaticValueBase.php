<?php
/**
 * @file
 * Contains \Drupal\entity_labels\Plugin\AutomaticValueBase.
 */

namespace Drupal\entity_labels\Plugin;

use Drupal\Component\Utility\String;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Plugin\PluginBase;

/**
 * Abstract base class for AutomaticValue plugins.
 */
abstract class AutomaticValueBase extends PluginBase implements AutomaticValueInterface {

  protected $entity_type_id;
  protected $bundle;
  protected $persistent;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    // Merge in default values and set plugin specific class variables.
    $this->setConfiguration($configuration);
  }

  /**
   * Returns the entity labels service.
   *
   * @return \Drupal\entity_labels\EntityLabelsManagerInterface
   */
  protected function entityLabels() {
    return \Drupal::service('entity_labels');
  }

  /**
   * {@inheritdoc}
   */
  public function applyAutomaticValue(ContentEntityInterface $entity) {
    $value = $this->getValue($entity);
    if ($value !== NULL) {
      $this->setLabelValue($entity, $value);
    }
  }

  /**
   * Returns the new label value for the given entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity.
   *
   * @return string
   *   The new label value, or NULL if there is no new value.
   */
  protected abstract function getValue(ContentEntityInterface $entity);

  /**
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   * @return \Drupal\Core\Field\FieldItemInterface
   * @throws Exception
   */
  protected function getLabelProperty(ContentEntityInterface $entity) {
    $label_property_name = $this->entityLabels()
      ->getPropertyName($entity->getEntityTypeId());
    $label_property = $entity->get($label_property_name);

    if ($label_property instanceof FieldItemListInterface) {
      $label_property = $label_property->first();
    }
    if ($label_property instanceof FieldItemInterface) {
      return $label_property;
    }
    else {
      // @todo proper exception?
      throw new Exception(String::format('Unsupported property class: %class', array('%class' => get_class($label_property))));
    }
  }

  /**
   * Helper function for setting the value of the label property on the given
   * content entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity.
   * @param string $value
   *   The new value.
   */
  protected function setLabelValue(ContentEntityInterface $entity, $value) {
    $label_property = $this->getLabelProperty($entity);

    // Determine maximum length and truncate value respectively.
    $max_length = $this->entityLabels()
      ->getLabelMaxLength($entity->getEntityTypeId());
    $value = $this->truncateValue($value, $max_length);

    // Set the label value.
    $main_property_key = $label_property->mainPropertyName();
    $label_property->get($main_property_key)->setValue($value);

    // @todo figure out how to properly handle violations.
    $violations = $label_property->validate();
    foreach ($violations as $violation) {
      \drupal_set_message($violation->getMessage(), 'error');
    }
  }

  /**
   * Truncate the given value to the given maximum length.
   *
   * Tries to not split up words and adds an ellipsis by default.
   *
   * @param string $value
   *   The value to truncate.
   * @param int $max_length
   *   The maximum length.
   *
   * @return string
   *   The truncated value.
   *
   * @todo Make paramters configurable (?)
   */
  protected function truncateValue($value, $max_length) {
    return \truncate_utf8($value, $max_length - 3, TRUE, TRUE, 1);
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    // Default implementation for label plugins without any settings.
    return array();
  }

  /**
   * {@inheritdoc}
   */
  public function getSettingsForm(array $form, array &$form_state) {
    // Default implementation for label plugins without any settings.
    return array();
  }

  /**
   * {@inheritdoc}
   */
  public function getBundleId() {
    return $this->bundle;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->pluginDefinition['description'];
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityTypeId() {
    return $this->entity_type_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getTypeId() {
    $id = $this->getEntityTypeId();
    if ($bundle_id = $this->getBundleId()) {
      $id .= '.' . $bundle_id;
    }
    return $id;
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel() {
    return $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function setBundleId($bundle) {
    $this->bundle = $bundle;
  }

  /**
   * {@inheritdoc}
   */
  public function setEntityTypeId($entity_type) {
    $this->entity_type_id = $entity_type;
  }

  /**
   * {@inheritdoc}
   */
  public function setSettings(array $settings) {
    $this->configuration = $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function isPersistent() {
    return $this->persistent;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return array(
      'type' => $this->getPluginId(),
      'entity_type' => $this->entity_type_id,
      'bundle' => $this->bundle,
      'settings' => $this->configuration,
    );
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $configuration += array(
      'entity_type' => NULL,
      'bundle' => NULL,
      'settings' => array(),
      'persistent' => FALSE,
    );
    $this->configuration = $configuration['settings'] + $this->defaultConfiguration();

    $this->entity_type_id = $configuration['entity_type'];
    $this->bundle = $configuration['bundle'];
    $this->persistent = $configuration['persistent'];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return array();
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    return array();
  }

}
