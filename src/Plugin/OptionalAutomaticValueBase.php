<?php
/**
 * @file
 * Contains \Drupal\entity_labels\Plugin\OptionalAutomaticValueBase.
 */

namespace Drupal\entity_labels\Plugin;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Abstract base class for OptionalAutomaticValue plugins.
 */
abstract class OptionalAutomaticValueBase extends AutomaticValueBase implements OptionalAutomaticValueInterface {

  /**
   * {@inheritdoc}
   */
  public function applyAutomaticValue(ContentEntityInterface $entity) {
    if ($this->isLabelEmpty($entity) || $this->overridesExistingValue()) {
      parent::applyAutomaticValue($entity);
    }
  }

  /**
   * Returns where the current label value of the given entity is empty.
   *
   * @param ContentEntityInterface $entity
   *   The entity.
   *
   * @return bool
   *   TRUE if the current label value is empty, FALSE otherwise.
   */
  public function isLabelEmpty(ContentEntityInterface $entity) {
    $label_property = $this->getLabelProperty($entity);
    $value = $label_property->get($label_property->mainPropertyName());
    return empty($value);
  }

  /**
   * {@inheritdoc}
   */
  public function overridesExistingValue() {
    return $this->configuration['override'];
  }

  /**
   * {@inheritdoc}
   */
  public function getSettingsForm(array $form, array &$form_state) {
    $element = parent::getSettingsForm($form, $form_state);
    $element['override'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Override existing entity labels'),
      '#default_value' => $this->overridesExistingValue(),
      '#description' => $this->t('When this option is disabled label values are only filled in if the label value is empty, for example if left empty during creation of a new entity.'),
    );
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return array(
      'override' => TRUE,
    );
  }

}
