<?php
/**
 * @file
 * Contains Drupal\entity_labels\Plugin\OptionalAutomaticValueInterface.
 */

namespace Drupal\entity_labels\Plugin;

/**
 * Interface for automatic value plugins which allow to choose whether to
 * override existing values when an entity is updated.
 */
interface OptionalAutomaticValueInterface extends AutomaticValueInterface {

  /**
   * Returns whether this automatic value plugins overrides existing label
   * values.
   *
   * @return bool
   *   TRUE if existing label values are overriden, FALSE if only empty labels
   *   are updated.
   */
  public function overridesExistingValue();
}
