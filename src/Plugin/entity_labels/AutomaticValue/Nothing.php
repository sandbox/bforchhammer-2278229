<?php
/**
 * @file
 * Contains \Drupal\entity_labels\Plugin\entity_labels\AutomaticValue\Nothing.
 */

namespace Drupal\entity_labels\Plugin\entity_labels\AutomaticValue;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\entity_labels\Plugin\AutomaticValueBase;

/**
 * Defines an automatic value plugin which does nothing.
 *
 * @AutomaticValue(
 *   id = "nothing",
 *   label = @Translation("No automatic value"),
 *   description = @Translation("Does not change the value of the entity label.")
 * )
 */
class Nothing extends AutomaticValueBase {

  /**
   * {@inheritdoc}
   */
  protected function getValue(ContentEntityInterface $entity) {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    $summary = array();
    $summary['#markup'] = $this->getLabel();
    return $summary;
  }

}
