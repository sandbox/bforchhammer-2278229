<?php
/**
 * @file
 * Contains Drupal\entity_labels\Plugin\entity_labels\AutomaticValue\TokenPattern.
 */

namespace Drupal\entity_labels\Plugin\entity_labels\AutomaticValue;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\entity_labels\Plugin\OptionalAutomaticValueBase;

/**
 * Defines an automatic value plugin which is based on text patterns with
 * tokens.
 *
 * @AutomaticValue(
 *   id = "token",
 *   label = @Translation("Token Pattern"),
 *   description = @Translation("Creates the label based on a text pattern with tokens.")
 * )
 *
 * @todo add sanitization options, e.g. trim and removal of double-spaces.
 */
class TokenPattern extends OptionalAutomaticValueBase {

  /**
   * Returns the token pattern.
   */
  public function getPattern() {
    return $this->configuration['pattern'];
  }

  /**
   * {@inheritdoc}
   */
  public function getValue(ContentEntityInterface $entity) {
    $token_data = array($this->entity_type_id => $entity);
    $token_options = array('langcode' => $entity->language()->getId());
    return \Drupal::token()
      ->replace($this->getPattern(), $token_data, $token_options);
  }

  /**
   * {@inheritdoc}
   */
  public function getSettingsForm(array $form, array &$form_state) {
    $element = parent::getSettingsForm($form, $form_state);

    $element['pattern'] = array(
      '#type' => 'textarea',
      '#title' => $this->t('Token Pattern'),
      '#rows' => 2,
      '#default_value' => $this->getPattern(),
      '#description' => $this->t('Enter the pattern for the entity label. [todo: integrate token browser once it is ready.'),
      '#required' => TRUE,
    );

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    return array(
      '#markup' => $this->t('Pattern: %pattern', array('%pattern' => $this->getPattern())),
    );
  }

  /**
   * {@inheritdoc}
   *
   * @todo remove test pattern.
   */
  public function defaultConfiguration() {
    return array(
      // @todo change to empty pattern!
      'pattern' => 'No pattern: [node:changed]',
    ) + parent::defaultConfiguration();
  }

}
