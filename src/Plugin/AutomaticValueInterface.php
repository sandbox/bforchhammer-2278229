<?php
/**
 * @file
 * Contains \Drupal\entity_labels\Plugin\AutomaticValueInterface.
 */

namespace Drupal\entity_labels\Plugin;

use Drupal\Component\Plugin\ConfigurablePluginInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Interface for AutomaticValue plugins.
 */
interface AutomaticValueInterface extends PluginInspectionInterface, ConfigurablePluginInterface {

  /**
   * Apply the automatic value to the given entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   */
  public function applyAutomaticValue(ContentEntityInterface $entity);

  /**
   * Returns the associated bundle id.
   *
   * @return string
   *   The bundle id.
   */
  public function getBundleId();

  /**
   * Returns a description for the automatic value plugin, for admin listings.
   *
   * @return string
   *   The description.
   */
  public function getDescription();

  /**
   * Returns the associated entity type id.
   *
   * @return string
   *   The entity type id.
   */
  public function getEntityTypeId();

  /**
   * Returns a string which uniquely identifies a plugin instance.
   *
   * @return string
   *   The id in form of 'entity_type' or 'entity_type.bundle'.
   */
  public function getTypeId();

  /**
   * Returns a label for the automatic value plugin, for admin listings.
   *
   * @return string
   *   The default value plugin label.
   */
  public function getLabel();

  /**
   * Returns a render array summarizing the configuration of the automatic value
   * plugin.
   *
   * @return array
   *   A render array.
   */
  public function getSummary();

  /**
   * Builds a settings form for configuring the automatic value plugin.
   *
   * @param array $form
   *   The form where the settings form is being included in.
   * @param array $form_state
   *   The form state of the (entire) configuration form.
   *
   * @return array
   *   The form definition for the plugin settings.
   */
  public function getSettingsForm(array $form, array &$form_state);

  /**
   * Set the bundle id.
   *
   * @param string $bundle
   *   The bundle id.
   */
  public function setBundleId($bundle);

  /**
   * Set the entity type id.
   *
   * @param string $entity_type
   *   The entity type id.
   */
  public function setEntityTypeId($entity_type);

  /**
   * Replace the plugin settings.
   *
   * This method is intended to be used by form submission handlers in
   * conjunction with getSettingsForm().
   *
   * @param array $settings
   *   The new settings.
   */
  public function setSettings(array $settings);

  /**
   * Returns whether this plugin was loaded from persistent storage.
   *
   * @return bool
   *   TRUE if this instance is stored in config, FALSE otherwise.
   */
  public function isPersistent();
}
