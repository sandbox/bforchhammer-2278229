<?php
/**
 * @file
 * Contains Drupal\entity_labels\Controller\EntityLabelsController class.
 */

namespace Drupal\entity_labels\Controller;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Utility\LinkGeneratorInterface;
use Drupal\entity_labels\EntityLabelsManagerInterface;
use Drupal\entity_labels\Plugin\AutomaticValueInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Builds the admin overview listing for entity labels.
 */
class EntityLabelsController implements ContainerInjectionInterface {
  use StringTranslationTrait;

  /**
   * The entity labels service.
   *
   * @var \Drupal\entity_labels\EntityLabelsManagerInterface
   */
  protected $entityLabels;

  /**
   * The link generator service.
   *
   * @var \Drupal\Core\Utility\LinkGeneratorInterface
   */
  protected $linkGenerator;

  /**
   * Constructs a new EntityLabelsController object.
   *
   * @param \Drupal\entity_labels\EntityLabelsManagerInterface $entityLabelsService
   *   The entity labels service.
   * @param \Drupal\Core\Utility\LinkGeneratorInterface
   *   The link generator service.
   */
  public function __construct(EntityLabelsManagerInterface $entityLabelsService, LinkGeneratorInterface $link_generator) {
    $this->entityLabels = $entityLabelsService;
    $this->linkGenerator = $link_generator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_labels'),
      $container->get('link_generator')
    );
  }

  /**
   * Returns the overview listing of entity labels and automatic values.
   *
   * @return array
   *   Render array.
   */
  public function adminOverview() {
    $page = array(
      '#attached' => array(
        'css' => array(
          \drupal_get_path('module', 'entity_labels') . '/css/entity_labels.admin.css',
        )
      )
    );

    $info = $this->entityLabels->getInfo();
    foreach ($info as $entity_type => $details) {
      $item = array(
        '#type' => 'details',
        '#title' => $details['label'],
        '#id' => $entity_type
      );

      $item['autovalues'] = array(
        '#type' => 'table',
        '#header' => array(
          t('Bundle'),
          t('Field Label'),
          t('Form display'),
          t('Automatic value'),
          t('Operations'),
        ),
      );
      // Add entity-type level automatic value info.
      $item['autovalues'][$entity_type] = $this->buildTableRow(
        $details['automatic value'],
        '<em>' . $this->t('All bundles') . '</em>',
        $details['operations'],
        $details['property']
      );
      // Add bundle-type level automatic value info
      foreach ($details['bundles'] as $bundle_id => $bundle) {
        $item['autovalues'][$entity_type . ':' . $bundle_id] = $this->buildTableRow(
          $bundle['automatic value'],
          $bundle['label'],
          $bundle['operations'],
          $bundle['property'],
          $bundle['form display']
        );
      }

      $page[$entity_type] = $item;
    }

    return $page;
  }

  /**
   * Builds one table row for the automatic-value table.
   *
   * @param AutomaticValueInterface $autovalue
   *   The automatic value.
   * @param $bundleLabel
   *   The value for the label column, i.e. bundle label or entity type label.
   * @param array $operations
   *   The link of operations; each link should be an array with 'title' and
   *   'href' key.
   * @param array $propertyInfo
   *   Information about the label property, e.g. the property 'label'.
   * @param array $formDisplay
   *   Information about how the label is displayed in forms.
   *
   * @return array
   *   One table row, matching the layout defined in adminOverview().
   */
  protected function buildTableRow(AutomaticValueInterface $autovalue, $bundleLabel, array $operations, array $propertyInfo, array $formDisplay = array()) {
    $row = array();
    $row['#attributes']['class'][] = $autovalue->isPersistent() ? 'automatic-value-persistent' : 'automatic-value-inherited';

    // Bundle column:
    $row['bundle'] = array(
      '#markup' => $bundleLabel,
    );

    // Property name column:
    $row['property_name'] = array(
      '#markup' => $propertyInfo['label'],
    );

    // Form display column:
    $numDisplays = count($formDisplay);
    $items = array();
    foreach ($formDisplay as $mode => $display) {
      $text = $display['hidden'] ? $this->t('Hidden') : $this->t('Visible');
      if (!empty($display['admin url'])) {
        $text = $this->linkGenerator->generateFromUrl($text, $display['admin url']);
      }
      if ($numDisplays != 1 || $mode != 'default') {
        $text = $display['label'] . ': ' . $text;
      }
      $items[] = $text;
    }
    if ($numDisplays == 1) {
      $row['form']['#markup'] = $items[0];
    }
    else {
      $row['form'][] = array(
        '#theme' => 'item_list',
        '#items' => $items,
      );
    }

    // Automatic value column:
    $row['value'] = $autovalue->getSummary();
    $row['value']['#wrapper_attributes']['class'][] = 'automatic-value-summary';

    // Operations column:
    $row['operations'] = array(
      '#type' => 'operations',
      '#links' => $operations,
      '#wrapper_attributes' => array('class' => array('operations')),
    );

    return $row;
  }
}
