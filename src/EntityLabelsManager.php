<?php
/**
 * @file
 * Contains Drupal\entity_labels\EntityLabelsManager.
 */

namespace Drupal\entity_labels;

use Drupal\Component\Utility\String;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Routing\UrlGeneratorInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\entity_labels\Exception\InvalidTypeException;

/**
 * Service implementation which provides information on entity labels.
 */
class EntityLabelsManager implements EntityLabelsManagerInterface {
  use StringTranslationTrait;

  /**
   * The entity manager service.
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  protected $entityManager;


  /**
   * The link generator service.
   *
   * @var UrlGeneratorInterface
   */
  protected $urlGenerator;

  /**
   * The automatic value plugin manager.
   *
   * @var \Drupal\entity_labels\AutomaticValueManagerInterface
   */
  protected $automaticValueManager;

  /**
   * Entity label related information.
   *
   * @var array
   */
  protected $info;

  /**
   * Constructs a new EntityLabelsManager object.
   *
   * @param EntityManagerInterface $entityManager
   *   The entity manager service.
   * @param AutomaticValueManagerInterface $automaticValueManager
   *   The automatic value plugin manager service.
   * @param UrlGeneratorInterface $url_generator
   *   The link generator service.
   */
  public function __construct(EntityManagerInterface $entityManager, AutomaticValueManagerInterface $automaticValueManager, UrlGeneratorInterface $url_generator) {
    $this->entityManager = $entityManager;
    $this->automaticValueManager = $automaticValueManager;
    $this->urlGenerator = $url_generator;
  }

  /**
   * @return ConfigTranslationMapper
   */
  public function configTranslationMapper() {
    static $instance = NULL;
    if (!$instance) {
      $mapperManager = \Drupal::service('plugin.manager.config_translation.mapper');
      $instance = $mapperManager->createInstance('entity_labels.automatic_value');
    }
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function automaticValues() {
    return $this->automaticValueManager;
  }

  /**
   * Returns the entity_type and bundle components of the given type.
   *
   * @param string $type
   *   The combined type id, e.g. '{entity_type}' or '{entity_type}.{bundle}'.
   *
   * @return array
   *   An array consisting of the entity type id and optionally the bundle id.
   */
  public static function explodeType($type) {
    $parts = explode('.', $type, 2);
    $parts = array_pad($parts, 2, '');
    return $parts;
  }

  /**
   * {@inheritdoc}
   */
  public function validateTypeId($type) {
    list ($entity_type_id, $bundle_id) = self::explodeType($type);

    // Check that we have a valid entity type id.
    if (!$this->entityManager->hasDefinition($entity_type_id)) {
      throw new InvalidTypeException(String::format("Invalid entity type @entity-type.", array('@entity-type' => $entity_type_id)));
    }

    // Check that we have a valid bundle id, if supplied.
    if ($bundle_id) {
      $bundleInfo = $this->entityManager->getBundleInfo($entity_type_id);
      if (!isset($bundleInfo[$bundle_id])) {
        throw new InvalidTypeException(String::format("Invalid bundle @bundle.", array('@bundle' => $bundle_id)));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getTypeLabel($type) {
    list ($entity_type, $bundle) = self::explodeType($type);
    $info = $this->getInfo($entity_type);
    if ($bundle) {
      return $info['bundles'][$bundle]['label'];
    }
    return $info['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function getPropertyName($type) {
    list ($entity_type) = self::explodeType($type);
    $info = $this->getInfo($entity_type);
    return $info['property']['name'];
  }

  /**
   * {@inheritdoc}
   */
  public function getPropertyLabel($type) {
    list ($entity_type, $bundle) = self::explodeType($type);
    $info = $this->getInfo($entity_type);
    if ($bundle) {
      return $info['bundles'][$bundle]['property']['label'];
    }
    return $info['property']['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function getLabelMaxLength($type) {
    list ($entity_type, $bundle) = self::explodeType($type);
    $info = $this->getInfo($entity_type);
    if ($bundle) {
      return $info['bundles'][$bundle]['property']['max length'];
    }
    return $info['property']['max length'];
  }

  /**
   * {@inheritdoc}
   */
  public function loadAutomaticValue($type, $inherit = TRUE) {
    list($entity_type, $bundle) = self::explodeType($type);
    return $this->automaticValues()->load($entity_type, $bundle, $inherit);
  }

  /**
   * {@inheritdoc}
   *
   * @todo implement caching
   */
  public function getInfo($entity_type = NULL) {
    if (empty($this->info)) {
      $this->info = $this->buildInfo();
    }
    if ($entity_type) {
      return isset($this->info[$entity_type]) ? $this->info[$entity_type] : FALSE;
    }
    return $this->info;
  }

  /**
   * Builds the entity labels information array.
   *
   * @return array
   *   An array with info related to entity labels. See getInfo() for structural
   *   details.
   */
  protected function buildInfo() {
    $info = array();

    $definitions = $this->entityManager->getDefinitions();
    foreach ($definitions as $definition) {
      // Only use content entity types.
      // @todo Configuration entity types should also work in theory, but they
      // are not our primary use case therefore we'll just ignore them for now.
      if (!$definition->isSubclassOf('\Drupal\Core\Entity\ContentEntityInterface')) {
        continue;
      }

      // Ignore entity types which do not have a label property.
      if (!$definition->hasKey('label')) {
        continue;
      }

      $info[$definition->id()] = $this->buildEntityTypeInfo($definition);
    }

    return $info;
  }

  /**
   * Builds the entity labels information array for the given entity type.
   *
   * @param EntityTypeInterface $entity_type
   *   The entity type.
   * @return array
   *   An array with info related to entity labels. See getInfo() for structural
   *   details.
   */
  protected function buildEntityTypeInfo(EntityTypeInterface $entity_type) {
    $label_property = $this->entityManager->getBaseFieldDefinitions($entity_type->id())[$entity_type->getKey('label')];

    $automatic_value = $this->automaticValueManager->load($entity_type->id());
    $info = array(
      'label' => $entity_type->getLabel(),
      'property' => $this->buildPropertyInfo($label_property) + array('name' => $entity_type->getKey('label')),
      'label callback' => $entity_type->getLabelCallback(),
      'automatic value' => $automatic_value,
      'operations' => $this->buildOperations($entity_type->id(), NULL, $automatic_value->isPersistent()),
      'bundles' => array(),
    );

    // Add bundle-specific information
    if ($entity_type->hasKey('bundle')) {
      $bundles = $this->entityManager->getBundleInfo($entity_type->id());
      foreach ($bundles as $key => $bundle) {
        $label_property_bundle = $this->entityManager->getFieldDefinitions($entity_type->id(), $key)[$entity_type->getKey('label')];

        $automatic_value = $this->automaticValueManager->load($entity_type->id(), $key);
        $bundle += array(
          'automatic value' => $automatic_value,
          'property' => $this->buildPropertyInfo($label_property_bundle),
          'form display' => $this->buildFormDisplayInfo($entity_type, $key, $label_property_bundle),
          'operations' => $this->buildOperations($entity_type->id(), $key, $automatic_value->isPersistent()),
        );
        $info['bundles'][$key] = $bundle;
      }
    }

    return $info;
  }

  protected function buildPropertyInfo(FieldDefinitionInterface $label_property) {
    $label_property_item = $label_property->getItemDefinition();
    return array(
      'label' => $label_property->getLabel(),
      'max length' => $label_property_item->getSetting('max_length'),
      // @todo The following parameters have only been added for testing purposes; remove?
      'default value' => $label_property_item->getSetting('default_value'),
      'required' => $label_property->isRequired(),
      'computed' => $label_property->isComputed(),
      'form display configurable' => $label_property->isDisplayConfigurable('form'),
    );
  }

  protected function buildOperations($entity_type_id, $bundle, $persistent) {
    $ops = array();
    $route = $this->automaticValues()
      ->getConfigureRoute($entity_type_id, $bundle);
    $ops['configure'] = array(
      'title' => $this->t('Configure'),
      'href' => $this->urlGenerator->generateFromRoute($route['route_name'], $route['route_parameters'], $route['route_options']),
    );

    if ($persistent) {
      $route = $this->automaticValues()
        ->getResetRoute($entity_type_id, $bundle);
      $ops['reset'] = array(
        'title' => $this->t('Reset'),
        'href' => $this->urlGenerator->generateFromRoute($route['route_name'], $route['route_parameters'], $route['route_options']),
      );

      $typeId = $entity_type_id;
      if ($bundle) {
        $typeId .= '.' . $bundle;
      }
      $configTranslation = $this->configTranslationMapper();
      $configTranslation->setTypes($typeId);
      if ($configTranslation->hasTranslatable()) {
        $ops['translate'] = array(
          'title' => $this->t('Translate'),
          'href' => $configTranslation->getOverviewPath(),
        );
      }
    }

    return $ops;
  }

  /**
   * Gathers information about whether the label property is visible or hidden
   * on entity forms.
   *
   * @param EntityTypeInterface $entity_type
   *   The entity type.
   * @param $bundle
   *   The bundle.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $property
   *   The name/key of the label property.
   *
   * @return array
   *   An array keyed by form mode and with following key-value pairs:
   *   - hidden: TRUE if the label field is hidden on forms.
   *   - label: The admin-label of the view mode.
   *   - admin-url: The url for configuring the form fields.
   */
  protected function buildFormDisplayInfo(EntityTypeInterface $entity_type, $bundle, FieldDefinitionInterface $property) {
    $form_display = array();

    $form_modes = $this->entityManager->getFormModeOptions($entity_type->id());
    foreach ($form_modes as $mode => $label) {
      $entity_display = \entity_load('entity_form_display', $entity_type->id() . '.' . $bundle . '.' . $mode);

      // Skip form modes without display configuration.
      if (empty($entity_display)) {
        continue;
      }

      // Skip disabled form modes.
      if ($entity_display->status == FALSE) {
        continue;
      }

      // The field is hidden on the form if there are no display options. Also,
      // computed properties are always excluded.
      $display_options = $entity_display->getComponent($property->getName());

      $form_display[$mode] = array(
        'hidden' => empty($display_options) || $property->isComputed(),
        'label' => $label,
        'admin url' => $this->getFormDisplayConfigurationRoute($entity_type, $bundle, $mode),
      );
    }
    return $form_display;
  }

  /**
   * @param EntityTypeInterface $entity_type
   *   The entity type.
   * @param $bundle
   *   The bundle id.
   * @param string $mode
   *   The form mode.
   * @return array|null
   *
   * @todo Possibly remove, see https://drupal.org/node/2279619
   */
  protected function getFormDisplayConfigurationRoute(EntityTypeInterface $entity_type, $bundle, $mode = 'default') {
    // Routes are only built for fieldable entity types which have an
    // 'admin-form' link template.
    if ($entity_type->isFieldable() && $entity_type->hasLinkTemplate('admin-form')) {
      if ($mode == 'default') {
        return new Url('field_ui.form_display_overview_' . $entity_type->id(), array(
          $entity_type->getBundleEntityType() => $bundle
        ));
      }
      else {
        return new Url('field_ui.form_display_overview_form_mode_' . $entity_type->id(), array(
          $entity_type->getBundleEntityType() => $bundle,
          'form_mode_name' => $mode
        ));
      }
    }
    return NULL;
  }
}
