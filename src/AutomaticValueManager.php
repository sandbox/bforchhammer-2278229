<?php
/**
 * @file
 * Contains Drupal\entity_labels\AutomaticValueManager.
 */

namespace Drupal\entity_labels;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Utility\String;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\entity_labels\Plugin\AutomaticValueInterface;

/**
 * Manager for AutomaticValue plugins.
 */
class AutomaticValueManager extends DefaultPluginManager implements AutomaticValueManagerInterface {

  const CONFIG_PREFIX = 'entity_labels.autovalue.';
  const PLUGIN_INTERFACE = '\Drupal\entity_labels\Plugin\AutomaticValueInterface';

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * List of already instantiated AutomaticValue plugins.
   *
   * @var array
   */
  protected $instances = array();

  /**
   * Constructs the AutomaticValueManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler, ConfigFactoryInterface $config_factory) {
    parent::__construct('Plugin/entity_labels/AutomaticValue', $namespaces, $module_handler, 'Drupal\entity_labels\Annotation\AutomaticValue');

    $this->alterInfo('entity_labels_automatic_value_info');
    $this->setCacheBackend($cache_backend, 'entity_labels_automatic_value_plugins');
    $this->configFactory = $config_factory;
  }

  /**
   * Overrides PluginManagerBase::getInstance().
   *
   * @param array $options
   *   An array with the following key/value pairs:
   *   - entity_type (optional): The entity type id
   *   - bundle (optional): The bundle id.
   *   - inherit (optional): Whether to inherit settings from a more general
   *     context. If set to TRUE and bundle-specific configuration cannot be
   *     found, then we load plugin configuration from the entity-type only; if
   *     that also fails, we fall back to a default plugin configuration.
   *     Defaults to TRUE.
   *
   * @return \Drupal\entity_labels\Plugin\AutomaticValueInterface
   *   A automatic value plugin instance.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function getInstance(array $options) {
    $options += array(
      'entity_type' => NULL,
      'bundle' => NULL,
      'inherit' => TRUE
    );

    $config_key = $this->buildConfigKey($options);
    $instance_key = $config_key ? $config_key : 'default';

    if (empty($this->instances[$instance_key])) {
      $settings = array();

      // If we have a config key, try to load plugin configuration from config.
      if ($config_key) {
        $settings = (array) $this->configFactory->get($config_key)->get();
        // Indicate that we successfully loaded something from configuration
        // without inheriting any values.
        if (!empty($settings['type'])) {
          $settings['persistent'] = TRUE;
        }
        // If we did not load any settings, try again without the bundle id.
        elseif ($options['inherit']) {
          $config_key2 = $this->buildConfigKey($options, TRUE);
          if ($config_key2 && $config_key != $config_key2) {
            $settings = (array) $this->configFactory->get($config_key2)->get();
          }
        }
      }

      // Exit if we have not determined a valid plugin type now, and also don't
      // want to use default plugin configuration.
      if (empty($settings['type']) && empty($options['inherit'])) {
        return NULL;
      }

      // Merge in default settings.
      $settings += $this->getDefaultSettings($options);

      // Instantiate the plugin.
      $instance = $this->createInstance($settings['type'], $settings);

      if (is_subclass_of($instance, static::PLUGIN_INTERFACE)) {
        $this->instances[$instance_key] = $instance;
      }
      else {
        throw new InvalidPluginDefinitionException($instance_key, String::format('Class %class does not implement interface %interface', array(
          '%class' => get_class($instance),
          '%interface' => static::PLUGIN_INTERFACE
        )));
      }
    }
    return $this->instances[$instance_key];
  }

  /**
   * Creates a configuration key matching the given options.
   *
   * @param array $options
   *   An array with the following key/value pairs:
   *   - entity_type: The entity type id.
   *   - bundle (optional): The bundle id.
   * @param bool $ignore_bundle
   *   Whether to ignore the bundle option, no-matter its' value.
   *
   * @return string
   *   The configuration key, FALSE if given options are invalid.
   */
  protected static function buildConfigKey($options, $ignore_bundle = FALSE) {
    $entity_type = isset($options['entity_type']) ? $options['entity_type'] : NULL;
    $bundle = isset($options['bundle']) ? $options['bundle'] : NULL;

    // If bundle is the same as the entity type, we assume that there are no
    // bundles and therefore ignore the supplied bundle key.
    if ($ignore_bundle) {
      $bundle = NULL;
    }

    // Determine the configuration key.
    if ($entity_type && $bundle) {
      return static::CONFIG_PREFIX . $entity_type . '_' . $bundle;
    }
    // Alternatively load configuration based on just the entity type.
    else {
      if ($entity_type) {
        return static::CONFIG_PREFIX . $entity_type;
      }
    }
    return FALSE;
  }

  /**
   * Creates a configuration key matching the given options.
   *
   * @param string $entity_type_id
   *   The entity type id.
   * @param string $bundle
   *   The bundle id.
   * @param bool $ignore_bundle
   *   Whether to ignore the bundle option, no-matter its' value.
   *
   * @return string
   *   The configuration key, FALSE if given options are invalid.
   */
  public static function getConfigKey($entity_type_id, $bundle = NULL, $ignore_bundle = FALSE) {
    $options = array(
      'entity_type' => $entity_type_id,
      'bundle' => $bundle,
    );
    return self::buildConfigKey($options, $ignore_bundle);
  }

  /**
   * Returns default plugin settings.
   *
   * @param array $options
   *   An array with the following key/value pairs:
   *   - entity_type: The entity type id.
   *   - bundle (optional): The bundle id.
   *
   * @return array
   *   Plugin configuration.
   */
  protected function getDefaultSettings($options) {
    return array(
      'type' => 'nothing',
      'entity_type' => isset($options['entity_type']) ? $options['entity_type'] : NULL,
      'bundle' => isset($options['bundle']) ? $options['bundle'] : NULL,
    );
  }

  /**
   * {@inheritdoc}
   */
  public function create($type, $entity_type, $bundle = NULL) {
    $configuration = array(
      'entity_type' => $entity_type,
      'bundle' => $bundle,
    );
    return $this->createInstance($type, $configuration);
  }

  /**
   * {@inheritdoc}
   */
  public function load($entity_type, $bundle = NULL, $inherit = TRUE) {
    $options = array(
      'entity_type' => $entity_type,
      'bundle' => $bundle,
      'inherit' => $inherit,
    );
    return $this->getInstance($options);
  }

  /**
   * {@inheritdoc}
   */
  public function save(AutomaticValueInterface $instance) {
    $options = array(
      'entity_type' => $instance->getEntityTypeId(),
      'bundle' => $instance->getBundleId(),
    );
    $config_key = $this->buildConfigKey($options);

    if (!empty($config_key)) {
      $config = $this->configFactory->get($config_key);
      $config->setData($instance->getConfiguration());
      $config->save();
      $this->invalidateFieldCache($instance->getEntityTypeId(), $instance->getBundleId());
      return TRUE;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function delete(AutomaticValueInterface $instance) {
    $options = array(
      'entity_type' => $instance->getEntityTypeId(),
      'bundle' => $instance->getBundleId(),
    );
    $config_key = $this->buildConfigKey($options);

    if (!empty($config_key)) {
      $config = $this->configFactory->get($config_key);
      $config->delete();
      $this->invalidateFieldCache($instance->getEntityTypeId(), $instance->getBundleId());
      return TRUE;
    }
    return FALSE;
  }

  protected function invalidateFieldCache($entity_type_id, $bundle) {
    \entity_labels_invalidate_field_definition_cache($entity_type_id, $bundle);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfigureRoute($entity_type_id, $bundle = NULL) {
    return $this->getRoute('configure', $entity_type_id, $bundle);
  }

  /**
   * {@inheritdoc}
   */
  public function getResetRoute($entity_type_id, $bundle = NULL) {
    return $this->getRoute('reset', $entity_type_id, $bundle);
  }

  /**
   * Returns route information.
   *
   * The route name is constructed as <code>entity_labels.autovalue.$op</code>. If $bundle is not empty, then
   * <code>.bundle</code> is appended at the end.
   *
   * @param string $op
   *   The route operation.
   * @param string $entity_type_id
   *   The entity type id.
   * @param string $bundle
   *   The bundle id (optional).
   *
   * @return array
   *   An associative array with the following keys:
   *   - route_name: The name of the route.
   *   - route_parameters: (optional) An associative array of parameter names
   *     and values.
   *   - route_options: (optional) An associative array of additional options. See
   *     \Drupal\Core\Routing\UrlGeneratorInterface::generateFromRoute()
   *     for comprehensive documentation.
   */
  protected function getRoute($op, $entity_type_id, $bundle = NULL) {
    $type_id = $entity_type_id;
    if ($bundle) {
      $type_id .= '.' . $bundle;
    }
    $route = array(
      'route_name' => 'entity_labels.autovalue.' . $op,
      'route_parameters' => array('type' => $type_id),
      'route_options' => array(),
    );
    return $route;
  }

}
