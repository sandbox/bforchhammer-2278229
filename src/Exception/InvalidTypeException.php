<?php
/**
 * @file
 * Contains Drupal\entity_labels\Exception\InvalidTypeException.
 */

namespace Drupal\entity_labels\Exception;

/**
 * Exception thrown for invalid type information e.g. invalid entity type id or
 * bundle id.
 */
class InvalidTypeException extends \Exception {

  /**
   * {@inheritdoc}
   */
  public function __construct($message, $code = 0, \Exception $previous = NULL) {
    parent::__construct($message, $code, $previous);
  }
}
