<?php
/**
 * @file
 * Contains \Drupal\entity_labels\Form\AutomaticValueConfigForm.
 */

namespace Drupal\entity_labels\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Url;
use Drupal\entity_labels\EntityLabelsManager;
use Drupal\entity_labels\EntityLabelsManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configuration form for automatic value plugins.
 */
class AutomaticValueConfigForm extends FormBase {

  /**
   * The entity labels service.
   *
   * @var \Drupal\entity_labels\EntityLabelsManagerInterface
   */
  protected $entityLabels;

  /**
   * Construct a new instance of the DefaultValueForm.
   *
   * @param \Drupal\entity_labels\EntityLabelsManagerInterface $entityLabels
   *   The entity labels service.
   */
  public function __construct(EntityLabelsManagerInterface $entityLabels) {
    $this->entityLabels = $entityLabels;

    // Disable configuration overrides to make sure that config form is loaded
    // in the system language (i.e. english).
    // @todo It would probably be better to add a configurable language property to plugins and load based on that value.
    \Drupal::configFactory()->setOverrideState(FALSE);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_labels')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'entity_labels_autolabel_configure';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, array &$form_state, $type = NULL) {
    if (!isset($form_state['autovalue'])) {
      list($entity_type, $bundle) = EntityLabelsManager::explodeType($type);

      // Load any existing auto value plugin for the given entity type and
      // bundle.
      $form_state['autovalue'] = $this->entityLabels->loadAutomaticValue($type);
      $form_state['autovalue_resetable'] = $form_state['autovalue']->isPersistent();

      // Make sure that entity type and bundle match the current request; these
      // may defer from the loaded plugin if values were inherited from a parent
      // context.
      $form_state['autovalue']->setEntityTypeId($entity_type);
      $form_state['autovalue']->setBundleId($bundle);
    }

    // Process any existing values to make it easy to send parts of the form
    // back via ajax.
    if (!empty($form_state['values'])) {
      $this->processValues($form, $form_state);
    }

    $form['#title'] = $this->t('Configure %type automatic label', array(
      '%type' => $this->entityLabels->getTypeLabel($type)
    ));

    $form['type'] = array(
      '#type' => 'radios',
      '#title' => $this->t('Automatic Value for %property', array(
          '%property' => $this->entityLabels->getPropertyLabel($type)
        )),
      '#options' => array(),
      '#default_value' => $form_state['autovalue']->getPluginId(),
      '#ajax' => array(
        'callback' => array($this, 'getSettingsForm'),
        'wrapper' => 'settings-wrapper',
        'effect' => 'fade',
      ),
    );
    $types = $this->entityLabels->automaticValues()->getDefinitions();
    foreach ($types as $definition) {
      $id = $definition['id'];
      $form['type']['#options'][$id] = $definition['label'];
      $form['type'][$id]['#description'] = $definition['description'];
    }

    // Add plugin settings form.
    $form['settings'] = $form_state['autovalue']->getSettingsForm($form, $form_state);

    if (!empty($form['settings'])) {
      // Wrap plugin settings into details element.
      $form['settings'] += array(
        '#type' => 'details',
        '#open' => TRUE,
        '#title' => $this->t('Settings'),
        '#tree' => TRUE,
        '#states' => array(
          'visible' => array(
            ':input[name="type"]' => array('value' => $form_state['autovalue']->getPluginId()),
          ),
        )
      );
    }
    // Always add wrapper div for ajax.
    $form['settings'] += array(
      '#prefix' => '<div id="settings-wrapper">',
      '#suffix' => '</div>',
    );

    $form['actions'] = array('#type' => 'actions');
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Save configuration'),
      '#button_type' => 'primary',
      '#weight' => 5,
    );
    $form['actions']['cancel'] = array(
      '#type' => 'link',
      '#title' => $this->t('Cancel'),
      '#route_name' => 'entity_labels.overview',
      '#weight' => 10,
    );
    return $form;
  }

  /**
   * Ajax callback for returning the updated settings form after the plugin type
   * has been changed.
   */
  public function getSettingsForm(array $form, array &$form_state) {
    return $form['settings'];
  }

  /**
   * Processes the 'type' and updates `$form_state[auto_value]`.
   */
  protected function processValues(array $form, array &$form_state) {
    // If the plugin type was just changed, create a new instance of the updated
    // plugin type. Discard any settings in this case.
    if ($form_state['values']['type'] != $form_state['autovalue']->getPluginId()) {
      $form_state['autovalue'] = $this->entityLabels->automaticValues()->create(
        $form_state['values']['type'], $form_state['autovalue']->getEntityTypeId(), $form_state['autovalue']->getBundleId());
    }
    else {
      // Update plugin configuration with new settings.
      $settings = isset($form_state['values']['settings']) ? $form_state['values']['settings'] : array();
      $form_state['autovalue']->setSettings($settings);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, array &$form_state) {
    $op = isset($form_state['values']['op']) ? $form_state['values']['op'] : FALSE;

    // Updated $form_state[auto_value].
    $this->processValues($form, $form_state);

    // If the submit button was clicked, persist the updated or new plugin in
    // config and redirect to the overview.
    if ($op == $form['actions']['submit']['#value']) {
      $this->entityLabels->automaticValues()->save($form_state['autovalue']);

      $typeLabel = $this->entityLabels->getTypeLabel($form_state['autovalue']->getTypeId());
      $propertyLabel = $this->entityLabels->getPropertyLabel($form_state['autovalue']->getTypeId());
      drupal_set_message(t('Saved configuration of automatic value for %type %label.', array(
        '%type' => $typeLabel,
        '%label' => $propertyLabel
      )));

      $form_state['redirect_route'] = new Url('entity_labels.overview');
    }
  }

}
