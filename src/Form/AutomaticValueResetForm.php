<?php
/**
 * @file
 * Contains Drupal\entity_labels\Form\AutomaticValueConfigForm.
 */

namespace Drupal\entity_labels\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Url;
use Drupal\entity_labels\EntityLabelsManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Confirmation form for resetting automatic value configuration.
 */
class AutomaticValueResetForm extends ConfirmFormBase {

  /**
   * The entity labels service.
   *
   * @var \Drupal\entity_labels\EntityLabelsManagerInterface
   */
  protected $entityLabels;

  /**
   * Construct a new instance of the AutomaticValueResetForm.
   *
   * @param \Drupal\entity_labels\EntityLabelsManagerInterface $entityLabels
   *   The entity labels service.
   */
  public function __construct(EntityLabelsManagerInterface $entityLabels) {
    $this->entityLabels = $entityLabels;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_labels')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelRoute() {
    return new Url('entity_labels.overview');
  }

  private $typeLabel;
  private $propertyLabel;

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Do you really want to reset the automatic value configuration for %type %label?', array(
      '%type' => $this->typeLabel,
      '%label' => $this->propertyLabel
    ));
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'entity_labels_autolabel_reset';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, array &$form_state, $type = NULL) {
    $this->typeLabel = $this->entityLabels->getTypeLabel($type);
    $this->propertyLabel = $this->entityLabels->getPropertyLabel($type);

    $form_state['autovalue'] = $this->entityLabels->loadAutomaticValue($type, FALSE);

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, array &$form_state) {
    $this->entityLabels->automaticValues()->delete($form_state['autovalue']);

    drupal_set_message(t('Reset configuration of automatic value for %type %label.', array(
      '%type' => $this->typeLabel,
      '%label' => $this->propertyLabel
    )));

    $form_state['redirect_route'] = $this->getCancelRoute();
  }

}
