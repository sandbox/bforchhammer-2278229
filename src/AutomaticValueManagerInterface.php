<?php
/**
 * @file
 * Contains \Drupal\entity_labels\AutomaticValueManagerInterface
 */

namespace Drupal\entity_labels;

use Drupal\entity_labels\Plugin\AutomaticValueInterface;

/**
 * Plugin manager service for default value plugins.
 */
interface AutomaticValueManagerInterface {

  /**
   * Create a new plugin instance.
   *
   * @param string $type
   *   The plugin id.
   * @param string $entity_type
   *   The entity type id.
   * @param string $bundle
   *   The bundle id.
   *
   * @return \Drupal\entity_labels\Plugin\AutomaticValueInterface
   *   A new default value plugin instance of the requested type.
   */
  public function create($type, $entity_type, $bundle = NULL);

  /**
   * Load plugin settings for the given entity type and bundle id.
   *
   * If $inherit is set to TRUE, this is guaranteed to always return a
   * plugin instance: if bundle-specific configuration cannot be found, then we
   * load plugin configuration from the entity-type only; if that also fails, we
   * fall back to a default plugin configuration.
   *
   * @param string $entity_type
   *   The entity type id.
   * @param string $bundle
   *   The bundle id (optional).
   * @param bool $inherit
   *   Whether to inherit settings from a more general context (see above).
   *
   * @return \Drupal\entity_labels\Plugin\AutomaticValueInterface
   *   A default value plugin instance. NULL if $load_default is set to FALSE
   *   and no plugin instance was found in config.
   */
  public function load($entity_type, $bundle = NULL, $inherit = TRUE);

  /**
   * Save the given plugin instance to config.
   *
   * @param \Drupal\entity_labels\Plugin\AutomaticValueInterface $instance
   *   The plugin instance.
   *
   * @param boolean
   *   TRUE if the plugin was saved successfully, FALSE otherwise.
   */
  public function save(AutomaticValueInterface $instance);

  /**
   * Delete the given plugin instance from config.
   *
   * @param \Drupal\entity_labels\Plugin\AutomaticValueInterface $instance
   *   The plugin instance.
   *
   * @param boolean
   *   TRUE if the plugin was deleted successfully, FALSE otherwise.
   */
  public function delete(AutomaticValueInterface $instance);

  /**
   * Returns the route information for configuring plugin configuration for a
   * specific entity type and (optional) bundle.
   *
   * @param string $entity_type_id
   *   The entity type id.
   * @param string $bundle
   *   The bundle id.
   *
   * @return array
   *   An associative array with the following keys:
   *   - route_name: The name of the route.
   *   - route_parameters: (optional) An associative array of parameter names
   *     and values.
   *   - route_options: (optional) An associative array of additional options.
   *     See \Drupal\Core\Routing\UrlGeneratorInterface::generateFromRoute()
   *     for comprehensive documentation.
   */
  public function getConfigureRoute($entity_type_id, $bundle = NULL);

  /**
   * Returns the route information for resetting plugin configuration for a
   * specific entity type and (optional) bundle.
   *
   * @param string $entity_type_id
   *   The entity type id.
   * @param string $bundle
   *   The bundle id.
   *
   * @return array
   *   An associative array with the following keys:
   *   - route_name: The name of the route.
   *   - route_parameters: (optional) An associative array of parameter names
   *     and values.
   *   - route_options: (optional) An associative array of additional options.
   *     See \Drupal\Core\Routing\UrlGeneratorInterface::generateFromRoute()
   *     for comprehensive documentation.
   */
  public function getResetRoute($entity_type_id, $bundle = NULL);
}
