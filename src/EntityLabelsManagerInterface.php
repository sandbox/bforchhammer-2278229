<?php
/**
 * @file
 * Contains Drupal\entity_labels\EntityLabelsManagerInterface.
 */

namespace Drupal\entity_labels;

/**
 * Describes a service which provides information on entity labels.
 */
interface EntityLabelsManagerInterface {

  /**
   * Returns information about entity label properties.
   *
   * @return array
   *   [@todo description]
   */
  public function getInfo();

  /**
   * Returns the automatic value plugin manager.
   *
   * @return \Drupal\entity_labels\AutomaticValueManagerInterface
   *   The automatic value manager.
   */
  public function automaticValues();

  /**
   * Checks that the given type id is valid and throws exceptions in case of
   * error.
   *
   * @param string $type
   *   The combined type id, e.g. '{entity_type}' or '{entity_type}.{bundle}'.
   * @throws \Drupal\entity_labels\Exception\InvalidTypeException
   *   If the type id is invalid.
   */
  public function validateTypeId($type);

  /**
   * Returns a type label.
   *
   * If $bundle is set, the translated bundle label is used, otherwise the
   * translated entity type label.
   *
   * @param string $type
   *   The combined type id, e.g. '{entity_type}' or '{entity_type}.{bundle}'.
   *
   * @return string
   *   Either the bundle name or entity type label.
   */
  public function getTypeLabel($type);

  /**
   * Returns the label of the entity label property.
   *
   * @param string $type
   *   The combined type id, e.g. '{entity_type}' or '{entity_type}.{bundle}'.
   *
   * @return string
   *   The label for the label property, e.g. 'Title' for nodes, 'Subject' for
   *   comments etc.
   */
  public function getPropertyLabel($type);

  /**
   * Returns the name of the label property.
   *
   * @param string $type
   *   The combined type id, e.g. '{entity_type}' or '{entity_type}.{bundle}'.
   *
   * @return string
   *   The name of the label property.
   */
  public function getPropertyName($type);

  /**
   * Returns the maximum length of the label value.
   *
   * @param string $type
   *   The combined type id, e.g. '{entity_type}' or '{entity_type}.{bundle}'.
   *
   * @return integer
   *   The maximum length of the label value.
   */
  public function getLabelMaxLength($type);

  /**
   * Load an automatic value plugin.
   *
   * @param string $type
   *   The combined type id, e.g. '{entity_type}' or '{entity_type}.{bundle}'.
   * @param bool $inherit
   *   Whether to inherit settings from a more general context.
   *
   * @return \Drupal\entity_labels\Plugin\AutomaticValueInterface
   *   A default value plugin instance. NULL if $load_default is set to FALSE
   *   and no plugin instance was found in config.
   *
   * @see AutomaticValueManagerInterface::load()
   */
  public function loadAutomaticValue($type, $inherit = TRUE);
}
